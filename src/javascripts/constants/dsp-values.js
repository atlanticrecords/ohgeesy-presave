const artist = 'Roddy Ricch';
const albumName = 'Late At Night';

const preorderConfig = {
  preorderCopy: `and get digital downloads of [TITLE] and This Song instantly.`,
  streamCopy: 'to get [TITLE OF SONG] added to your playlists and/or pre-save THE ALBUM NAME for release day.',
  preorderDsps: [
    //{
    //  label: 'Merchandise Bundle',
    //  clickTrack: 'Bundle Pre-sale Click',
    //  iconClass: 'atl-icon atl-icon--shopping-cart',
    //  url: 'http://smarturl.it/PNBRockStore?intcmp=190416/pnbrock/atl/lan/s_pr/but/bdy/ww/trapstar-turnt-popstar-linkfire-plus-pre-sale-bundle'
    //},
    {
      label: 'iTunes',
      dataTrack: 'itunes',
      iconClass: 'atl-icon atl-icon--apple',
      url: 'https://music.apple.com/us/album/late-at-night-single/1570462257?ls=1&app=itunes',
    },
    //{
    //  label: 'Amazon',
    //  dataTrack: 'amazon-p',
    //  iconClass: 'atl-icon atl-icon--amazon',
    //  url: 'https://PNBrock.lnk.to/TrapStarTurnPopStarPreOrderSP/amazonmp3',
    //},
    //{
    //  label: 'Google Play',
    //  dataTrack: 'google-play',
    //  iconClass: 'atl-icon atl-icon--google-play',
    //  url: 'https://PNBrock.lnk.to/TrapStarTurnPopStarPreOrderSP/google-play',
    //},
  ],
  streamDsps: [
    {
      name: 'spotify',
      label: 'Spotify',
      iconClass: 'atl-icon atl-icon--spotify',
      dataTrack: 'spotify-presave',
      actionLabel: 'Pre-save & Follow',
      actionDescription: 'Connect with Spotify and follow Artist, pre-save \'track\' and sign up for email updates',
    },
    {
      name: 'apple',
      label: 'Apple Music',
      iconClass: 'atl-icon atl-icon--apple',
      dataTrack: 'apple-music-presave',
      actionLabel: 'Pre-save & Follow',
      actionDescription: 'Connect with Apple Music and follow Artist, pre-add \'track\' and sign up for email updates',
    },
    //{
    //  name: 'amazon',
    //  label: 'Amazon Music',
    //  iconClass: 'atl-icon atl-icon--amazon',
    //  actionLabel: 'Stream',
    //  streamUrl: 'https://www.amazon.com/Kanye-West/e/B000APR990',
    //},
    {
      name: 'google',
      label: 'Youtube',
      iconClass: 'atl-icon atl-icon--youtube',
      dataTrack: 'youtube',
      actionLabel: 'Follow Artist!',
      actionDescription: 'Connect with Youtube and follow Artist!',
      streamUrl: 'https://www.youtube.com/ohgeesy?sub_confirmation=1',
    },
    //{
    //  name: 'tidal',
    //  label: 'Tidal',
    //  iconClass: 'atl-icon atl-icon--tidal',
    //  actionLabel: 'Stream',
    //  streamUrl: 'https://listen.tidal.com/artist/25022',
    //},
    //{
    //  name: 'deezer',
    //  label: 'Deezer',
    //  iconClass: 'atl-icon atl-icon--deezer',
    //  actionLabel: 'Save & Follow',
    //  actionDescription: 'Connect with Deezer and follow Artist, pre-save \'track\' and sign up for email updates',
    //},
  ],
  streamDspLinks: {
    'youtube-music': 'https://PNBrock.lnk.to/TrapStarTurnPopStarPreOrderSP/youtube',
    amazon: 'https://PNBrock.lnk.to/TrapStarTurnPopStarPreOrderSP/amazonmp3',
    tidal: 'https://listen.tidal.com/playlist/10e38d5a-e6b8-4918-89c4-84a1b1702ad2',
  },
  dspConnect: {
    spotify: 'Connect with Spotify to follow ' + artist + ', save \'' + albumName + '\' and be automatically subscribed to receive email marketing messages.',
    apple: 'Connect with Apple Music to follow ' + artist + ', Add \'' + albumName + '\' and stay tuned for updates from ' + artist + '!',
    google: 'Connect with Youtube to follow ' + artist + ' and be automatically subscribed to receive email marketing messages.',
    deezer: 'Connect with Deezer to follow ' + artist + ', save \'' + albumName + '\' and be automatically subscribed to receive email marketing messages.',
  }
};

export default preorderConfig;
