import { loadImage } from './load-images';
import { fabric } from 'fabric';

const lockObject = {
  lockMovementX: true,
  lockMovementY: true,
  hasControls: false,
  hasBorders: false,
};

const fabricLoadImageAsync = url =>
  new Promise(resolve => {
    fabric.Image.fromURL(url, img => resolve(img));
  });

const fabricAddBgImg = async options => {
  const { canvas, src } = options;
  const img = await fabricLoadImageAsync(src);

  const scaleRatio = img.height / img.width;
  img.scaleToWidth(canvas.width);
  img.scaleToHeight(canvas.width * scaleRatio);
  canvas.setBackgroundImage(img);
};

const fabricAddHoverableImage = async options => {
  const { canvas, label, left, top, src, hoverSrc, onMouseOver, onMouseOut, onClick } = options;
  const img = await fabricLoadImageAsync(src);
  const imgEl = img.getElement();
  const imgHover = await loadImage(hoverSrc);

  img.set({ ...lockObject, hoverCursor: 'pointer', left, top });
  img.label = label;
  canvas.add(img);

  canvas.on('mouse:over', e => {
    if (e.target && e.target.label === label) {
      e.target.setElement(imgHover);
      canvas.requestRenderAll();
      if (onMouseOver) {
        onMouseOver();
      }
    }
  });

  canvas.on('mouse:out', e => {
    if (e.target && e.target.label === label) {
      e.target.setElement(imgEl);
      canvas.requestRenderAll();
      if (onMouseOut) {
        onMouseOut();
      }
    }
  });

  canvas.on('mouse:down', e => {
    if (e.target && e.target.label === label) {
      onClick();
    }
  });
};

export { fabricLoadImageAsync, fabricAddBgImg, fabricAddHoverableImage, lockObject };
