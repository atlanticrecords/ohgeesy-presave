import { open } from 'utils/popup';
import {
  SPOTIFY_CONFIG,
  APPLE_CONFIG,
  YOUTUBE_CONFIG,
  DEEZER_CONFIG
} from 'utils/config';

// Spotify const
export const SPOTIFY_AUTH_ENDPOINT = 'https://accounts.spotify.com/authorize';
export const SPOTIFY_REDIRECT_URI = `${location.protocol}//${location.host}${location.pathname}spotify_callback.html`;
export const DEEZER_REDIRECT_URI = `${location.protocol}//${location.host}${location.pathname}deezer_channel.html`;

if (APPLE_CONFIG.clientId) {
  document.addEventListener('musickitloaded', () => {
    MusicKit.configure({
      developerToken: APPLE_CONFIG.clientId,
      app: {
        name: 'ATLREC Sandbox',
        build: '0.0.1'
      }
    });
  });
}

if (YOUTUBE_CONFIG.clientId) {
  window.handleYoutubeClient = () => {
    gapi.load('client:auth2', () => {
      gapi.client.init({
        discoveryDocs: YOUTUBE_CONFIG.discovery,
        clientId: YOUTUBE_CONFIG.clientId,
        scope: YOUTUBE_CONFIG.scope,
      });
    });
  };
}

if (DEEZER_CONFIG.clientId) {
  DZ.init({
    appId: DEEZER_CONFIG.clientId,
    channelUrl: DEEZER_REDIRECT_URI,
  });
}


// Spotify Auth
const spotifyLogin = () => {

  // Catch missing required config settings
  if (!SPOTIFY_CONFIG.clientId) {
    throw new Error('Missing Spotify client ID');
  }

  if (!SPOTIFY_CONFIG.scope) {
    throw new Error('Missing Spotify scope');
  }

  const url = `${SPOTIFY_AUTH_ENDPOINT}?client_id=${SPOTIFY_CONFIG.clientId}&redirect_uri=${encodeURIComponent(SPOTIFY_CONFIG.redirect_uri || SPOTIFY_REDIRECT_URI)}&scope=${encodeURIComponent(SPOTIFY_CONFIG.scope.join(' '))}&response_type=token`;

  return new Promise((resolve, reject) => {
    window.addEventListener('message', ((event) => {
      try {
        const hash = JSON.parse(event.data);

        if (hash.type === 'access_token') {
          resolve(hash.access_token);
        } else {
          reject('Access token missing');
        }
      } catch (error) {}

    }), false);

    // Open auth in popup
    open(url, 'Spotify', 450, 730, true);
  });
};

// Apple Auth
const appleLogin = () => {

  // Catch missing required config settings
  if (!APPLE_CONFIG.clientId) {
    throw new Error('Missing Apple client ID');
  }

  if (!MusicKit) {
    throw new Error('Missing Apple MusicKit');
  }

  return MusicKit.getInstance().authorize();
};

// YoutTube Auth
const youtubeLogin = () => {

  // Catch missing required config settings
  if (!YOUTUBE_CONFIG.clientId) {
    throw new Error('Missing YouTube client ID');
  }

  return gapi.auth2.getAuthInstance().signIn();
};

// Deezer Auth
const deezerLogin = () => {

  // Catch missing required config settings
  if (!DEEZER_CONFIG.clientId) {
    throw new Error('Missing Deezer client ID');
  }

  if (!DEEZER_CONFIG.scope) {
    throw new Error('Missing Deezer scope');
  }

  return new Promise((resolve, reject) => {
    DZ.login(response => {
      if (response.authResponse) {
        resolve(response.authResponse.accessToken);
      } else {
        reject('Access token missing');
      }
    }, { perms: DEEZER_CONFIG.scope });
  });
};

export default {};
export { spotifyLogin, appleLogin, youtubeLogin, deezerLogin };
