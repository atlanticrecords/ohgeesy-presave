import React from 'react';

const FirstNameField = () => (
  <div className="mlist-field mlist-field--first-name">
    <label htmlFor="firstname">First Name</label>
    <input id="firstname" type="text" name="firstname" placeholder="First Name" />
    <div className="messages" />
  </div>
);

export default FirstNameField;
