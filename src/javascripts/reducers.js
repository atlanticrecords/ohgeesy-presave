import { combineReducers } from 'redux';

import modal from 'components/modal/reducer';
import device from 'components/layout/device/reducer';
import topsify from 'components/topsify-auth-btn/reducer';
import dspConnectReducer from 'components/dsp-connect-dialog/reducer';

const rootReducer = combineReducers({
  topsify,
  dspConnectDialog: dspConnectReducer,
  ui: combineReducers({
    modal,
    device,
  }),
});

export default rootReducer;
