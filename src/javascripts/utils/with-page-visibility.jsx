import React from 'react';
import PageVisibility from 'react-page-visibility';

const withPageVisibility = Component => props => (
  <PageVisibility>{isVisible => <Component {...props} isVisible={isVisible} />}</PageVisibility>
);

export default withPageVisibility;
