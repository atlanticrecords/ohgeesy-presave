import createAction from 'utils/create-action';
import { trackSignup } from 'utils/tracking';
import { scrapeEmail } from 'utils/mailing-list';

const actions = {
  AUTHORIZED_SUCCESS: 'AUTHORIZED_SUCCESS',
  AUTHORIZED_FAIL: 'AUTHORIZED_FAIL',
  EMAIL_AUTHORIZED_SUCCESS: 'EMAIL_AUTHORIZED_SUCCESS',
  EMAIL_AUTHORIZED_FAIL: 'EMAIL_AUTHORIZED_FAIL',
};

const authorizedSuccess = createAction(actions.AUTHORIZED_SUCCESS);
const authorizedFail = createAction(actions.AUTHORIZED_FAIL);
const emailAuthorizedSuccess = createAction(actions.EMAIL_AUTHORIZED_SUCCESS);

const emailSignup = email => async dispatch => {
  await scrapeEmail({ email });
  emailAuthorizedSuccess({ email })(dispatch);
  trackSignup();
};

const setAuthorized = ({ authorized }) => dispatch => {
  if (authorized) {
    authorizedSuccess()(dispatch);
  } else {
    authorizedFail()(dispatch);
  }
};

export default actions;
export { emailSignup, setAuthorized };
