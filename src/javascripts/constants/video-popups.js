import Product from 'components/product';

const v1 = [
  {
    id: 0,
    title: 'Bhad Bhabie',
    body:
      'If I have slides on, and I’m gonna fight someone, I’m just gonna take ‘em off. And I wouldn’t fight no one in a dress. I did get in a fight and I had my whole tank top ripped. I had a sports bra on, so it was good, but the whole tank top ripped. I was like, “Bitches always need to hold on to something. Just take your ass beating and be okay with it. There’s no way out of it, why you gonna rip shit?”',
    visible: false,
    showAt: 5,
    hideAt: 0,
  },
  {
    id: 1,
    title: 'Bhad Bhabie',
    body:
      'I know how to drive, but I can’t legally drive. I’m too lazy to get a fucking permit, I don’t want to go anywhere anyways. It’s not like I can go nowhere without Frank anyway, so it’s like, might as well just fucking wait. But I am a good driver, okay? Give me a fucking chance, okay? Every time I’ve drove with someone in the car, they’ve been scared at first, and then they were like, “Oh, you’re actually not that bad.” It’s like, “Yeah, bitch. What’d you think I was gonna do? Fucking drive on the wrong side of the road? I don’t know what’s supposed to happen here.”',
    visible: false,
    showAt: 10,
    hideAt: 0,
  },
  {
    id: 2,
    title: 'Bhad Bhabie',
    body:
      'If I have slides on, and I’m gonna fight someone, I’m just gonna take ‘em off. And I wouldn’t fight no one in a dress. I did get in a fight and I had my whole tank top ripped. I had a sports bra on, so it was good, but the whole tank top ripped. I was like, “Bitches always need to hold on to something. Just take your ass beating and be okay with it. There’s no way out of it, why you gonna rip shit?”',
    visible: false,
    showAt: 15,
    hideAt: 0,
  },
  {
    id: 3,
    title: 'Bhad Bhabie',
    body:
      'I know how to drive, but I can’t legally drive. I’m too lazy to get a fucking permit, I don’t want to go anywhere anyways. It’s not like I can go nowhere without Frank anyway, so it’s like, might as well just fucking wait. But I am a good driver, okay? Give me a fucking chance, okay? Every time I’ve drove with someone in the car, they’ve been scared at first, and then they were like, “Oh, you’re actually not that bad.” It’s like, “Yeah, bitch. What’d you think I was gonna do? Fucking drive on the wrong side of the road? I don’t know what’s supposed to happen here.”',
    visible: false,
    showAt: 20,
    hideAt: 0,
  },
];

const v2 = [
  {
    id: 0,
    title: 'Charli XCX',
    body: Product,
    visible: false,
    showAt: 2,
    hideAt: 0,
    style: {
      top: '2%',
      left: '2%',
    },
  },
  {
    id: 1,
    title: 'Charli XCX',
    body:
      'The reason we filmed this scene was because the my friend Channy didn’t have running water in her apartment. She was really dank and needed a good washing',
    visible: false,
    showAt: 5,
    hideAt: 0,
    style: {
      top: '52%',
      left: '5%',
    },
  },
  {
    id: 2,
    title: 'Charli XCX',
    body:
      'When you’re at a party and you’re pretty messed up and you just aren’t really sure how many people you got with. You know? Everyone’s been there. Maybe. Right? I don’t know. And you’re like, “Oh yeah, that one dude. Oh no. There were three different people who kind of looked similar,” or something like that.',
    visible: false,
    showAt: 8,
    hideAt: 0,
    style: {
      top: '5%',
      right: '5%',
    },
  },
];

export { v1, v2 };
