import { connect } from 'react-redux';
import ContestForm from './contest-form';
import { MLIST_CONFIG } from 'utils/config';

const mapStateToProps = state => ({
  newsletterId: MLIST_CONFIG.id,
  datasource: MLIST_CONFIG.datasource,
  ext: MLIST_CONFIG.ext,
  trigger: MLIST_CONFIG.trigger,
  artist: 'A R I Z O N A',
  privacyUrl: 'http://www.atlanticrecords.com/privacy-policy',
  termsUrl: 'http://www.atlanticrecords.com/terms-of-use',
  rulesUrl: './find_someone_game_sweepstakes_rules.pdf',
  pronoun: 'their',
  gameWon: state.game.gameWon,
});

export default connect(mapStateToProps)(ContestForm);
