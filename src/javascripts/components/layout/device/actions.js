const actions = {
  TOUCH_START: 'TOUCH_START',
};

const touchStart = () => dispatch => dispatch({ type: actions.TOUCH_START });

export default actions;
export { touchStart };
