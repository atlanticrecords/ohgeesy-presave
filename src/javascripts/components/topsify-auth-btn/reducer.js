import actions from './actions';

const initialState = {
  authorized: false,
  service: null,
  id: null,
  token: null,
  email: null,
  username: null,
};

const topsifyReducer = (state = initialState, action) => {
  const { type, payload } = action;
  switch (type) {
    case actions.SPOTIFY_AUTHORIZED_SUCCESS:
      return { ...state, ...payload, authorized: true, service: 'spotify' };
    case actions.SPOTIFY_AUTHORIZED_FAIL:
      return { ...state, authorized: false };
    case actions.APPLE_AUTHORIZED_SUCCESS:
      return { ...state, ...payload, authorized: true, service: 'apple' };
    case actions.APPLE_AUTHORIZED_FAIL:
      return { ...state, authorized: false };
    default:
      return state;
  }
};

export default topsifyReducer;
