import React from 'react';
import Socials from 'components/socials';
import TopsifyAuthBtn from 'components/topsify-auth-btn';
import './hello-world.scss';

const HelloWorld = ({ showModal }) => {
  const c = 'class-test';

  return (
    <section>
      <h1>Hello World!</h1>
      <Socials />
      <TopsifyAuthBtn btnText="Start" service="spotify" useIcon onAuthSuccess={() => console.log('auth success')} />
      <button
        className={`atl-${c}--test adjacent-class`}
        type="button"
        style={{ color: 'green' }}
        onClick={() => showModal({ type: 'HelloWorld'})}
      >
        Open Modal
      </button>
    </section>
  );
};

export default HelloWorld;
