const shuffleArray = array => {
  const clone = array.slice(0);
  for (let i = clone.length - 1; i > 0; i--) {
    const j = Math.floor(Math.random() * (i + 1));
    [clone[i], clone[j]] = [clone[j], clone[i]]; // eslint-disable-line no-param-reassign
  }
  return clone;
};

export default shuffleArray;
