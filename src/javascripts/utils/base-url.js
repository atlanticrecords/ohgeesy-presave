const parsedURL = new URL(window.location.href);
const baseURL = parsedURL.origin;
export default baseURL;
export { parsedURL };
