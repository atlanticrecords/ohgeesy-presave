import React, { useEffect, useState } from 'react';
import camelToSnake from 'utils/camel-to-snake';
import PropTypes from 'prop-types';
import modalTypes from 'constants/modal-types';

const Modal = ({ type, hideModal, closeButton }) => {
  const [ModalComponent, setModalComponent] = useState(() => false);
  useEffect(() => {
    if (type.length > 0) setModalComponent(modalTypes[type]);
  }, [type]);

  return (
    <div className="modal">
      <div className="modal__content">{ModalComponent && <ModalComponent />}</div>
      {closeButton && (
        <button
          className="modal__close-button"
          custom-link-name="Close Lightbox"
          onClick={() => hideModal()}
        >
          <i className="atl-icon atl-icon--close" />
        </button>
      )}
    </div>
  );
};

Modal.defaultProps = {
  type: '',
  closeButton: true,
};

Modal.propTypes = {
  type: PropTypes.string,
  hideModal: PropTypes.func.isRequired,
  closeButton: PropTypes.bool,
};

export default Modal;
