import { connect } from 'react-redux';

import { showModal } from 'components/modal/actions';
import FinishImageToggle from './finish-image-toggle';

const mapDispatchToProps = dispatch => ({
  showFinishImage: () => dispatch(showModal({ type: 'ShareImage' })),
});

export default connect(
  null,
  mapDispatchToProps
)(FinishImageToggle);
