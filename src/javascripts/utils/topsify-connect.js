import { open } from 'utils/popup';

const topsifyConnect = (campaignId, platform) => {
  try {
    const baseUrl = 'https://campaigns.topsify.com/';
    const buttonUrls = {
      spotify: baseUrl + 'app/connect2/spotify?campaign=' + campaignId + '&embed_widget=true',
      deezer: baseUrl + 'app/connect2/deezer?campaign=' + campaignId + '&embed_widget=true',
      google: baseUrl + 'app/connect2/google?campaign=' + campaignId + '&embed_widget=true',
      apple: baseUrl + 'app/connect2/apple?campaign=' + campaignId + '&embed_widget=true'
    };

    const selected_list_ids_url_parts = [];

    const url = buttonUrls[platform] + '&user_agreed_to_terms_and_conditions=1' + (selected_list_ids_url_parts.length ? '&' + selected_list_ids_url_parts.join('&') : '');

    open(url, 'connect', 500, 600);
  } catch (e) {
    console.error(e);
  }
};

export default topsifyConnect;
