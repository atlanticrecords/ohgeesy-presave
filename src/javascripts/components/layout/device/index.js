import { connect } from 'react-redux';
import Device from './device';

import { touchStart } from './actions';

const mapStateToProps = state => ({
  isTouchDevice: state.ui.device.touch,
});

const mapDispatchToProps = dispatch => ({
  touchStart: () => dispatch(touchStart()),
});

export default connect(mapStateToProps, mapDispatchToProps)(Device);
