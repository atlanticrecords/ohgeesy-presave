import { createStore, applyMiddleware, compose } from 'redux';
import thunk from 'redux-thunk';
import { persistStore, persistReducer } from 'redux-persist';
import localForage from 'localforage';
import rootReducer from '../reducers';

const persistConfig = {
  key: 'root',
  storage: localForage,
  whitelist: ['user'],
};

const persistedReducer = persistReducer(persistConfig, rootReducer);

const configureStore = () => {
  const enhancers = compose(
    applyMiddleware(thunk),
    window.__REDUX_DEVTOOLS_EXTENSION__ ? window.__REDUX_DEVTOOLS_EXTENSION__() : compose // eslint-disable-line no-underscore-dangle
  );

  const store = createStore(persistedReducer, enhancers);
  const persistor = persistStore(store);

  return { store, persistor };
};

export default configureStore;
