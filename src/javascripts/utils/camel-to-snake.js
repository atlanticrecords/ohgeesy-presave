export default string => string.split(/(?=[A-Z])/).join('_').toLowerCase();
