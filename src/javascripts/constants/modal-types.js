//import HelloWorld from 'components/hello-world';
import DspConnectDialog from 'components/dsp-connect-dialog';

const modalTypes = {
  DspConnectDialog,
};

export default modalTypes;
