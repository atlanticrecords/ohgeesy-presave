import React from 'react';

const PostalCodeField = () => (
  <div className="mlist-field mlist-field--postal-code">
    <label htmlFor="postalcode">Postal/Zipcode</label>
    <input id="postalcode" type="text" name="postalcode" placeholder="Zipcode" />
    <div className="messages" />
  </div>
);

export default PostalCodeField;
