import React from 'react';
import PropTypes from 'prop-types';
import { MLIST_CONFIG } from 'utils/config';

const PrimaryValues = ({newsletterId, datasource, ext, trigger, children}) => (
  <div className="mlist-field hidden" id="mlist-primary-values" style={{ display: 'none' }}>
    <input id="newsletterId" type="hidden" name="newsletterId" value={newsletterId} />
    <input id="datasource" type="hidden" name="Datasource" value={datasource} />
    {ext ? <input id="ext" type="hidden" name="_ext" value={ext} /> : null}
    {trigger ? <input id="trigger" type="hidden" name="_trigger" value={trigger} /> : null}
    {children}
  </div>
);

PrimaryValues.defaultProps = {
  newsletterId: MLIST_CONFIG.id,
  datasource: MLIST_CONFIG.datasource,
};

PrimaryValues.propTypes = {
  newsletterId: PropTypes.string.isRequired,
  datasource: PropTypes.string.isRequired,
  ext: PropTypes.string,
  trigger: PropTypes.string,
  children: PropTypes.node,
};

export default PrimaryValues;
