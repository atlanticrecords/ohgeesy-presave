import React, { Fragment } from 'react';
import PropTypes from 'prop-types';
import camelToSentence from 'utils/camel-to-sentence';
import { PAGE_CONFIG } from 'utils/config';

const Footer = ({ copyright, privacy, termsOfUse, adChoices, cookiePolicy }) => (
  <footer className="primary-footer p-3">
    <div className="legal">
      <span className="copyright">{copyright}</span>
      <div className="footer-links">
        {Object.entries({ privacy, termsOfUse, adChoices, cookiePolicy }).map(
          ([name, link], index) => (
            <Fragment key={index}>
              {index !== 0 && <span> | </span>}
              <a
                className="footer-link"
                href={link}
                target="_blank"
                rel="noreferrer noopener"
              >
                {camelToSentence(name)}
              </a>
            </Fragment>
          )
        )}
      </div>
    </div>
  </footer>
);

Footer.defaultProps = {
  copyright: PAGE_CONFIG.copyright,
  privacy: PAGE_CONFIG.privacy,
  termsOfUse: PAGE_CONFIG.termsOfUse,
  adChoices: PAGE_CONFIG.adChoices,
  cookiePolicy: PAGE_CONFIG.cookiePolicy,
};

Footer.propTypes = {
  copyright: PropTypes.string.isRequired,
  privacy: PropTypes.string.isRequired,
  termsOfUse: PropTypes.string.isRequired,
  adChoices: PropTypes.string.isRequired,
  cookiePolicy: PropTypes.string.isRequired,
};

export default Footer;
