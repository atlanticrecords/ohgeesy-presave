function open(url, name, width, height, center) {
  const offset = {
    top: 0,
    left: 0,
  };

  if (center) {
    offset.left = window.screen.width / 2 - width / 2;
    offset.top = window.screen.height / 2 - height / 2;
  }

  return window.open(
    url,
    name,
    `menubar=no,
    location=no,
    resizable=no,
    scrollbars=no,
    status=no,
    width=${width},
    height=${height},
    top=${offset.top},
    left=${offset.left}`
  );
}

export default {};
export { open };
