import camelToSnake from './camel-to-snake';

export default (state, namespace) =>
  Object.fromEntries(
    Object.keys(state).map(key => [
      `UPDATE_${camelToSnake(key).toUpperCase()}`,
      `atl/${namespace}/UPDATE_${camelToSnake(key).toUpperCase()}`,
    ])
  );
