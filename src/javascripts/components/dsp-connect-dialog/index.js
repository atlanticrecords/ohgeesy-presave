import { connect } from 'react-redux';
import DspConnectDialog from './dsp-connect-dialog';
import { connectToPlatform } from './actions';
import preorderConfig from 'constants/dsp-values';

const { streamDsps } = preorderConfig;

const mapStateToProps = state => ({
  platform: streamDsps.filter(dsp => dsp.name === state.dspConnectDialog.platform)[0],
  connectSuccess: state.dspConnectDialog.connectSuccess,
  //iconClass: state.dspConnectDialog.iconClass,
});

const mapDispatchToProps = dispatch => ({
  connectToPlatform: platform => dispatch(connectToPlatform(platform)),
});

export default connect(mapStateToProps, mapDispatchToProps)(DspConnectDialog);
