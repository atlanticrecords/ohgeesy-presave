<?php
$container = $app->getContainer();

$container->add('Illuminate\Database\Capsule\Manager', function() {
  $capsule = new \Illuminate\Database\Capsule\Manager;
  $capsule->addConnection([
    'driver' => 'mysql',
    'host' => getenv('MYSQL_HOST'),
    'database' => getenv('MYSQL_DATABASE'),
    'username' => getenv('MYSQL_USER'),
    'password' => getenv('MYSQL_PASSWORD'),
    'charset'   => 'utf8',
    'prefix'    => '',
  ]);

  $capsule->setAsGlobal();
  $capsule->bootEloquent();

  return $capsule;
});
