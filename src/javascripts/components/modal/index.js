import { connect } from 'react-redux';

import Modal from './modal';
import { hideModal } from './actions';
import './style.scss';

const mapStateToProps = state => ({
  type: state.ui.modal.type,
  closeButton: true,
});

const mapDispatchToProps = dispatch => ({
  hideModal: () => dispatch(hideModal()),
});

export default connect(mapStateToProps, mapDispatchToProps)(Modal);
