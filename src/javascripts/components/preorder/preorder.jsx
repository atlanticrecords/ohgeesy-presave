import React, { Fragment, useEffect, useState } from 'react';
import axios from 'axios';
import { albumArt } from 'images';
import { trackClick, trackPage, trackDspClick } from 'utils/tracking';
import preorderConfig from 'constants/dsp-values';

const { streamDsps, preorderCopy, streamCopy, streamDspLinks } = preorderConfig;

const Preorder = ({ showModal, setPlatform }) => {
  const [preorderDsps, setPreorderDsps] = useState([]);
  const selectDsp = platform => {
    setPlatform(platform.name);
    trackPage(`${platform.label}:LightBox`, 'Lightbox');
    showModal({ type: 'DspConnectDialog' });
  };

  useEffect(() => {
    (async () => {
      const res = await axios.get('dsp-values.json');
      console.log(res.data);
      setPreorderDsps(res.data.preorderConfig.preorderDsps);
    })();
  }, []);

  return (
    <div className="container py-5">
      <div className="row">
        <div className="col-md-7 mb-4 mb-md-0">
          <img className="img-fluid" src={albumArt} alt="album art" />
        </div>
        <div className="col-md-5">
          <section className="section section--stream-follow-save">
            <header className="section__header">
              <h3 className="h3 text-uppercase">Stream/Follow/Save</h3>
              <p className="stream-copy">{streamCopy}</p>
            </header>
            <ul className="dsp-list">
              {streamDsps.map(dsp => (
                <li key={dsp.label}>
                  {dsp.streamUrl ? (
                    <a
                      className="button--stream"
                      href={dsp.streamUrl}
                      target="_blank"
                      rel="noopener noreferrer"
                      {...(dsp.dataTrack ? { 'data-track': dsp.dataTrack } : { onClick: () => trackDspClick(dsp.clickTrack) })}
                    >
                      <i className={dsp.iconClass} />
                      <span className="label">{dsp.label}</span>
                      <i className="atl-icon atl-icon--external" />
                    </a>
                  ) : (
                    <button
                      className="button--stream"
                      onClick={() => selectDsp(dsp)}
                      {...(dsp.dataTrack ? { 'data-track': dsp.dataTrack } : {})}
                    >
                      <i className={dsp.iconClass} />
                      <span className="label">{dsp.label}</span>
                      <i className="atl-icon atl-icon--view-more" />
                    </button>
                  )}
                </li>
              ))}
            </ul>
          </section>
          <section className="section section--preorder">
            <header className="section__header">
              <h3 className="h3 text-uppercase">PRE-ORDER Now</h3>
              <p className="preorder-copy">{preorderCopy}</p>
            </header>
            <ul className="dsp-list">
              {preorderDsps.map(dsp => (
                <li key={dsp.label}>
                  <a
                    href={dsp.url}
                    className="button--stream"
                    target="_blank"
                    rel="noopener noreferrer"
                    {...(dsp.dataTrack ? { 'data-track': dsp.dataTrack } : { onClick: () => trackClick(dsp.clickTrack) })}
                  >
                    <i className={dsp.iconClass} />
                    <span className="label">{dsp.label}</span>
                    <i className="atl-icon atl-icon--external" />
                  </a>
                </li>
              ))}
            </ul>
          </section>
        </div>
      </div>
      <div className="row mt-5">
        <div className="col-12">
          <div className="embed-responsive embed-responsive-16by9">
            <iframe
              width="560"
              height="315"
              src="https://www.youtube-nocookie.com/embed/432kcoNiASo?enablejsapi=1"
              title="YouTube video player"
              frameBorder="0"
              allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture"
              allowFullScreen
            />
          </div>
        </div>
      </div>
      <div className="row mt-5">
        <div className="col-12">
          <a
            className="btn btn-primary"
            href="https://www.geezyworld.com/"
            target="_blank"
            custom-link-name="View Merch Button"
          >
            VIEW MERCH
          </a>
        </div>
      </div>
    </div>
  );
};

export default Preorder;
