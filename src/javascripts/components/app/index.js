import { hot } from 'react-hot-loader';
import { connect } from 'react-redux';
import { compose } from 'redux';

import App from './app';

const mapStateToProps = state => ({
  showModal: state.ui.modal.show,
});

const mapDispatchToProps = dispatch => ({
});

export default compose(
  hot(module),
  connect(
    mapStateToProps,
    mapDispatchToProps
  )
)(App);
