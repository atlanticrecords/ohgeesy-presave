const loadImage = image =>
  new Promise(resolve => {
    const img = new Image();
    const isImageObject = typeof image === 'object';
    const imageCopy = image.slice();
    img.onload = () => {
      if (isImageObject) {
        imageCopy.el = img;
        resolve(image);
      } else {
        resolve(img);
      }
    };
    img.onerror = () => resolve({ image, status: 'error' });

    img.src = isImageObject ? imageCopy.src : image;
  });

const loadImages = images => Promise.all(images.map(loadImage));

export { loadImage, loadImages };
export default loadImages;
