import React, { Fragment } from 'react';
import PropTypes from 'prop-types';
import SignupTerms from './signup-terms';

const ContestTerms = ({ rules }) => (
  <div className="terms--contest-terms terms">
    Yes, I agree to the&nbsp;
    <a href={rules} target="_blank" rel="noopener noreferrer">
      submission agreement and official rules
    </a>
    . <SignupTerms />
  </div>
);

ContestTerms.defaultProps = {
  rules: '#',
};

ContestTerms.propTypes = {
  rules: PropTypes.string.isRequired,
};

export default ContestTerms;
