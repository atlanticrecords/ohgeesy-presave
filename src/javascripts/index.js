import initReactApp, { createRootElement } from './html';
import '../stylesheets/style.scss';

createRootElement();
initReactApp();
