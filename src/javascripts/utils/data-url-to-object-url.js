const dataURLToObjectURL = uri =>
  fetch(uri)
    .then(res => res.blob())
    .then(blob => URL.createObjectURL(blob));

export default dataURLToObjectURL;
