import createAction from 'utils/create-action';
import { trackSignup } from 'utils/tracking';
import { scrapeEmail } from 'utils/mailing-list';

const services = ['spotify', 'apple'];

const actions = {
  SPOTIFY_AUTHORIZED_SUCCESS: 'SPOTIFY_AUTHORIZED_SUCCESS',
  SPOTIFY_AUTHORIZED_FAIL: 'SPOTIFY_AUTHORIZED_FAIL',
  APPLE_AUTHORIZED_SUCCESS: 'APPLE_AUTHORIZED_SUCCESS',
  APPLE_AUTHORIZED_FAIL: 'APPLE_AUTHORIZED_FAIL',
};

const spotifyAuthorizedSuccess = createAction(actions.SPOTIFY_AUTHORIZED_SUCCESS);
const appleAuthorizedSuccess = createAction(actions.APPLE_AUTHORIZED_SUCCESS);

try {
  window.wmgConnect = new window.WMGConnect(false, {
    opt_in_thank_you_enabled: false,
    opt_in_dialogue_enabled: false,
  });
} catch (e) {
  if (!window.atlConfig.vendors.topsifyId) {
    console.error('No Topsify Campaign ID provided, please create campaign');
  }
  console.error(e);
}

const fetchTopsifyLogin = ({ service, onAuthSuccess }) => async dispatch => {
  try {
    if (!services.includes(service)) {
      throw Error(`Please provide one of the following services: ${services.toString()}`);
    }
    window.wmgConnect.startProcess(service);

    window.wmgConnect.setCallback(async data => {
      try {
        if (data.service) {
          console.log(data);
          if (data.service === 'spotify') {
            const { id, email, name, spotify_access_token, uri } = data.user;
            const username = uri.replace('spotify:user:', '');
            onAuthSuccess();
            spotifyAuthorizedSuccess({ id, email, username, token: spotify_access_token })(dispatch);
            scrapeEmail({ email });
            trackSignup('email sign-up');
          } else if (data.service === 'apple') {
            const { id, apple_music_access_token } = data.user;
            onAuthSuccess();
            appleAuthorizedSuccess({ id, token: apple_music_access_token })(dispatch);
          }
        }
      } catch (error) {
        console.error(`Topsify Auth Btn Callback ${error}`);
      }
    });
  } catch (error) {
    if (service === 'spotify') {
      dispatch({ type: actions.SPOTIFY_AUTHORIZED_FAIL });
    } else if (service === 'apple') {
      dispatch({ type: actions.APPLE_AUTHORIZED_FAIL });
    }

    console.error(`Topsify Auth Btn ${error}`);
  }
};

export default actions;
export { fetchTopsifyLogin };
