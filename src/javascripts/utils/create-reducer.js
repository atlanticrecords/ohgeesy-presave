import createReducerActions from './create-reducer-actions';
import createActionCreators from './create-action-creators';

export default (stateData, namespace, canReset = true) => {
  const reducerActions = createReducerActions(stateData, namespace);
  const actions = createActionCreators(reducerActions);

  const initialState = Object.fromEntries(
    Object.keys(stateData).map(k => [
      k,
      stateData[k].hasOwnProperty('default') ? stateData[k].default : stateData[k],
    ])
  );
  //console.log(reducerActions);
  return {
    actions,
    reducer: (state = initialState, action) => {
      const { type, payload } = action;
      if (canReset && type === 'atl/RESET') {
        return initialState;
      } else if (Object.keys(reducerActions).find(k => reducerActions[k] === type)) {
        return { ...state, ...payload };
      }
      return state;
    },
  };
};
