import createAction from 'utils/create-action';
import { scrapeEmail } from 'utils/mailing-list';
import { trackSignup } from 'utils/tracking';

const actions = {
  SET_PLATFORM: 'SET_PLATFORM',
  PLATFORM_CONNECT_START: 'PLATFORM_CONNECT_START',
  PLATFORM_CONNECT_SUCCESS: 'PLATFORM_CONNECT_SUCCESS',
  PLATFORM_CONNECT_FAIL: 'PLATFORM_CONNECT_FAIL',
};

const wmgConnect = new window.WMGConnect(false, {
  opt_in_thank_you_enabled: false,
  opt_in_dialogue_enabled: false,
});

const setPlatform = createAction(actions.SET_PLATFORM);

const connectToPlatform = platform => dispatch => {
  dispatch({ type: actions.PLATFORM_CONNECT_START });
  try {
    wmgConnect.startProcess(platform.name);

    wmgConnect.setCallback(async data => {
      try {
        if (data.service) {
          console.log(data);
          if (data.service === 'spotify') {
            scrapeEmail(data.user.email);
            trackSignup('email sign-up');
            trackSignup('music pre-save');
          }
          dispatch({ type: actions.PLATFORM_CONNECT_SUCCESS });
        }
      } catch (e) {}
    });
  } catch (error) {
    dispatch({ type: actions.PLATFORM_AUTHORIZED_FAIL });
  }
};

export default actions;
export { setPlatform, connectToPlatform };
