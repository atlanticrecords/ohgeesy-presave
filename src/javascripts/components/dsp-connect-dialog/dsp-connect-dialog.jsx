import React, { Fragment, useEffect, useState, useRef } from 'react';
import { albumArt2 } from 'images';
import { SignupTerms, ToggleTerms } from 'components/mailing-list/terms';
import { trackClick, resetPageData } from 'utils/tracking';
import preorderConfig from 'constants/dsp-values';
import './dsp-connect-dialog.scss';

const { dspConnect, preorderDsps, streamDsps, preorderCopy, streamCopy, streamDspLinks } = preorderConfig;

const DspConnectDialog = ({ platform, connectSuccess, connectToPlatform }) => {
  const [termsAgreed, setTermsAgreed] = useState(false);
  const [showError, setShowError] = useState(false);
  const container = useRef(null);

  useEffect(() => {
    return () => resetPageData();
  }, []);

  const handleCheckbox = e => {
    setTermsAgreed(e.target.checked);
    return showError && setShowError(false);
  };

  const handleSubmit = service => {
    if (termsAgreed) {
      console.log(service);
      connectToPlatform(service);
      trackClick(service.authTrack);
    } else {
      setShowError(true);
    }
  };

  return (
    <div className="dsp-connect-dialog" ref={container}>
      <div className="wrapper">
        <img src={albumArt2} className="album-art" alt="Ugly Dolls Soundtrack Album Art" />
      </div>
      {connectSuccess && <p>Thanks!</p>}
      {!connectSuccess && platform.actionDescription && (
        <Fragment>
          <div className="wrapper--terms">
            <p className="text-primary">{dspConnect[platform.name]}</p>
            <div className="terms-agree">
              <label className="custom-checkbox" htmlFor={`${platform.name}-terms-agree-checkbox`}>
                <input
                  type="checkbox"
                  id={`${platform.name}-terms-agree-checkbox`}
                  checked={termsAgreed}
                  onChange={handleCheckbox}
                  name="terms-agree-checkbox"
                />
                <span className="checkmark" />
              </label>
              <p className="terms-agree__body">
                <SignupTerms />
              </p>
              <p
                className="terms-agree__error"
                style={showError ? { display: 'block' } : { display: 'none' }}
              >
                You must agree to the terms to continue
              </p>
            </div>
          </div>
          <button
            className="button--stream"
            onClick={() => handleSubmit(platform)}
            {...(platform.dataTrack ? { 'data-track': platform.dataTrack } : {})}
          >
            <span className="label">{platform.actionLabel}</span>
            <i className="atl-icon atl-icon--external" />
          </button>
        </Fragment>
      )}
      {!connectSuccess && platform.streamUrl && (
        <a
          href={platform.streamUrl}
          className="button--stream"
          target="_blank"
          rel="noopener noreferrer"
          //onClick={() => trackClick(`${platform.label}:Follow Click`)}
        >
          <span className="label">{platform.actionLabel}</span>
          <i className="atl-icon atl-icon--external" />
        </a>
      )}
    </div>
  );
};

export default DspConnectDialog;
