import React from 'react';

import InfoToggle from './info-toggle';
import InspirationToggle from './inspiration-toggle';

const ModalToggles = () => (
  <ul className="modal-toggles">
    <li className="modal-toggles__item">
      <InfoToggle />
    </li>
    <li className="modal-toggles__item">
      <InspirationToggle />
    </li>
  </ul>
);

export default ModalToggles;
