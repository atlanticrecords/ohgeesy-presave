import actions from './actions';
import modalActions from 'components/modal/actions';

const initialState = {
  platform: null,
  connectSuccess: false,
};

const dspConnectDialogReducer = (state = initialState, action) => {
  switch (action.type) {
    case actions.SET_PLATFORM:
        return { ...state, platform: action.payload };
    case actions.PLATFORM_CONNECT_SUCCESS:
        return { ...state, connectSuccess: true };
    case modalActions.MODAL_HIDE:
        return { ...state, connectSuccess: false };
    default:
        return state;
  }
};

export default dspConnectDialogReducer;
