import React, { Fragment } from 'react';
import { EmailField, FirstNameField, PostalCodeField, CountryField, BirthdateField, PrimaryValues, SubmitBtn } from 'components/mailing-list/fields';
import USTermsAgree from '../../terms/us-terms-agree';
import './contest-form.scss';

const ContestForm = () => (
  <Fragment>
    <EmailField />
    <FirstNameField />
    <PostalCodeField />
    <CountryField />
    <BirthdateField />
    <PrimaryValues />
    <USTermsAgree />
    <SubmitBtn />
  </Fragment>
);

export default ContestForm;
