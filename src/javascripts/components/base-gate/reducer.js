import actions from './actions';

const initialState = {
  authorized: false,
  email: null,
};

const baseGateReducer = (state = initialState, action) => {
  switch (action.type) {
    case actions.AUTHORIZED_SUCCESS:
      return { ...state, ...action.payload, authorized: true };
    case actions.AUTHORIZED_FAIL:
      return { ...state, authorized: false };
    case actions.EMAIL_AUTHORIZED_SUCCESS:
      return { ...state, ...action.payload, authorized: true };
    case actions.EMAIL_AUTHORIZED_FAIL:
      return { ...state, authorized: false };
    default:
      return state;
  }
};

export default baseGateReducer;
