<?php

use Slim\Http\Request;
use Slim\Http\Response;

// Routes
$app->post('/save-image', function (Request $request, Response $response, array $args) {
  $directory = $this->get('upload_directory');
  $controller = $this->get('App\Controller\Image');
  $result = $controller->saveImage($request, $directory);

  $response->getBody()->write(json_encode($result));
  return $response->withStatus(200);
});

// Lazy schema stuff
$app->get('/up', function (Request $request, Response $response, array $args) {
  $controller = $this->get('App\Controller\Heart');
  $controller->up();
});

$app->get('/down', function (Request $request, Response $response, array $args) {
  $controller = $this->get('App\Controller\Heart');
  $controller->down();
});
