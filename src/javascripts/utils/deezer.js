import { DEEZER_CONFIG } from 'utils/config';
import { scrapeEmail } from 'utils/mailing-list';

const defaults = {
  followArtists: true,
  followPlaylist: true,
  scrapeEmail: true,
};

class Deezer {
  constructor(settings) {
    this.settings = { ...defaults, ...settings };
    this.api = DZ.api;
    this.init();
  }

  init() {
    // Follow artist
    if (DEEZER_CONFIG.artistId && this.settings.followArtists) {
      this.api('/user/me/artists', 'POST', { artist_id : DEEZER_CONFIG.artistId }, res => {});
    }

    // Follow playist
    if (DEEZER_CONFIG.playlistId && this.settings.followPlaylist) {
      this.api('/user/me/playlists', 'POST', { playlist_id : DEEZER_CONFIG.playlistId }, res => {});
    }


    // Mailing list
    if (this.settings.scrapeEmail) {
      this.api('/user/me/', user => scrapeEmail(user.email));
    }
  }
}

export default Deezer;
