import { connect } from 'react-redux';
import { compose } from 'redux';
import withLayout from 'components/layout/with-layout';
import { showModal } from 'components/modal/actions';
import { setPlatform } from 'components/dsp-connect-dialog/actions';
import Preorder from './preorder';
import './preorder.scss';

const mapDispatchToProps = dispatch => ({
  setPlatform: platform => dispatch(setPlatform(platform)),
  showModal: modal => dispatch(showModal(modal)),
});

export default compose(
  connect(null, mapDispatchToProps),
  withLayout
)(Preorder);
