import { connect } from 'react-redux';

import { showModal } from 'components/modal/actions';
import InfoToggle from './info-toggle';

const mapDispatchToProps = dispatch => ({
  showInfo: () => dispatch(showModal({ type: 'About' })),
});

export default connect(null, mapDispatchToProps)(InfoToggle);
