import React from 'react';

const Header = () => (
  <header className="primary-header mb-3">
    <span className="h2 h2-responsive">Pre-save</span>
    <h1 className="h1 h1-responsive mb-0 text-primary">GEEZYWORLD</h1>
    <span className="h2 h2-responsive">Available Everywhere 8.13</span>
  </header>
);

export default Header;
